`timescale 1 ns / 1 ps
`include "system.vh"
`include "interconnect.vh"

module system 
  (
   input                  clk,
   input                  reset,
   output                 trap,

`ifdef USE_DDR //AXI MASTER INTERFACE

   //address write
   output [0:0]           m_axi_awid, 
   output [`ADDR_W-1:0]   m_axi_awaddr,
   output [7:0]           m_axi_awlen,
   output [2:0]           m_axi_awsize,
   output [1:0]           m_axi_awburst,
   output [0:0]           m_axi_awlock,
   output [3:0]           m_axi_awcache,
   output [2:0]           m_axi_awprot,
   output [3:0]           m_axi_awqos,
   output                 m_axi_awvalid,
   input                  m_axi_awready,

   //write
   output [`DATA_W-1:0]   m_axi_wdata,
   output [`DATA_W/8-1:0] m_axi_wstrb,
   output                 m_axi_wlast,
   output                 m_axi_wvalid, 
   input                  m_axi_wready,

   //write response
   input [0:0]            m_axi_bid,
   input [1:0]            m_axi_bresp,
   input                  m_axi_bvalid,
   output                 m_axi_bready,
  
   //address read
   output [0:0]           m_axi_arid,
   output [`ADDR_W-1:0]   m_axi_araddr, 
   output [7:0]           m_axi_arlen,
   output [2:0]           m_axi_arsize,
   output [1:0]           m_axi_arburst,
   output [0:0]           m_axi_arlock,
   output [3:0]           m_axi_arcache,
   output [2:0]           m_axi_arprot,
   output [3:0]           m_axi_arqos,
   output                 m_axi_arvalid, 
   input                  m_axi_arready,

   //read
   input [0:0]            m_axi_rid,
   input [`DATA_W-1:0]    m_axi_rdata,
   input [1:0]            m_axi_rresp,
   input                  m_axi_rlast, 
   input                  m_axi_rvalid, 
   output                 m_axi_rready,
`endif //  `ifdef USE_DDR

   //UART
   output                 uart_txd,
   input                  uart_rxd,
   output                 uart_rts,
   input                  uart_cts
   );

   //
   // SYSTEM RESET
   //
   wire                   soft_reset;   
   wire                   reset_int = reset | soft_reset;
   
   //
   //  CPU
   //

   // instruction bus
   wire [`REQ_W-1:0]      cpu_i_req;
   wire [`RESP_W-1:0]     cpu_i_resp;

   // data cat bus
   wire [`REQ_W-1:0]      cpu_d_req;
   wire [`RESP_W-1:0]     cpu_d_resp;


   // boot flag
`ifdef USE_BOOT
   wire                   boot;
`endif
   
   //instantiate the cpu
`ifdef PICORV32
   iob_picorv32 cpu
`elsif DARKRV
     iob_darkrv cpu
`endif
       (
        .clk     (clk),
        .rst     (reset_int),
        .trap    (trap),
        
        //instruction bus
        .ibus_req(cpu_i_req),
        .ibus_resp(cpu_i_resp),
        
        //data bus
        .dbus_req(cpu_d_req),
        .dbus_resp(cpu_d_resp)
        );


   //
   //INTERNAL MEMORY AND PERIPHERALS CAT BUSES
   //

   //internal memory instruction bus
   wire [`REQ_W-1:0]      int_mem_i_req;
   wire [`RESP_W-1:0]     int_mem_i_resp;
   //external memory instruction bus
   wire [`REQ_W-1:0]      ext_mem_i_req;
   wire [`RESP_W-1:0]     ext_mem_i_resp;

 
   //   
   // SPLIT CPU INSTRUCTION BUS INTO INTERNAL AND EXTERNAL MEMORY
   //

`ifdef RUN_DDR
   split
     ibus_demux
       (
        // master interface
        .m_req  (cpu_i_req),
        .m_resp (cpu_i_resp),
        
        // slaves interface
 `ifdef USE_BOOT
        .s_sel (~boot),
 `else
        .s_sel (1'b1),
 `endif
        .s_req ({ext_mem_i_req, int_mem_i_req}),
        .s_resp ({ext_mem_i_resp, 0), int_mem_i_resp})
       );
`else
   assign int_mem_req = cpu_i_req;
   assign cpu_i_resp = int_mem_i_resp;
`endif

   //   
   // SPLIT CPU DATA BUS INTO INTERNAL AND EXTERNAL MEMORY
   //

   //internal memory data bus
   wire [`REQ_W-1:0]      int_mem_d_req;
   wire [`RESP_W-1:0]     int_mem_d_resp;
   //external memory data bus
   wire [`REQ_W-1:0]      ex_mem_d_req;
   wire [`RESP_W-1:0]     ex_mem_d_resp;
   //peripheral bus
   wire [`REQ_W-1:0]      per_req;
   wire [`RESP_W-1:0]     per_resp;

   split 
     #(
`ifdef USE_DDR
     .N_SLAVES(3)
`else
       .N_SLAVES(2)
`endif
       )
   dmembus_demux
      
     (
     // master interface
     .m_req (cpu_d_req),
  .m_resp (cpu_d_resp),

  // slaves interface
`ifdef USE_DDR
       .sel(cpu_d_req[`addr_slave(2)]),
  .s_req ({ext_mem_d_req, per_req, int_mem_d_req}),
  .s_resp({ext_mem_d_resp, per_resp, int_mem_d_resp}}),
`else
       .sel(cpu_d_req[`addr_slave(1)]),
  .s_req ({per_req, int_mem_d_req}}),
  .s_resp({per_resp, int_mem_d_resp})
`endif
     );

 
   //   
   // SPLIT PERIPHERAL BUS
   //


   //slaves bus
   wire [`N_SLAVES*`REQ_W-1:0]      slaves_req;
   wire [`N_SLAVES*`RESP_W-1:0]     slaves_resp;

   
   // peripheral demux
   split 
     #(
     .N_SLAVES(`N_SLAVES)
       )
   per_demux
     (
      // master interface
      .m_req (per_d_req),
      .m_resp (per_resp),
      
      // slaves interface
     .sel(per_d_req[`address_nbits(`N_SLAVES)])
      .s_req (slave_req),
      .s_resp (slave_resp)
      );

   
   /////////////////////////////////////////////////////////////////////////
       // MODULE INSTANCES
   
   //
   // INTERNAL SRAM MEMORY
   //

   int_mem int_mem0 
     (
     .clk                  (clk ),
     .rst                  (reset_int),

     // instruction bus
     .i_req                (int_mem_i_req),
     .i_resp               (int_mem_i_resp),

     //data bus
     .d_req                (int_mem_d_req),
     .d_resp               (int_mem_d_resp)
       );


`ifdef `USE_DDR
   
    //
   // EXTERNAL DDR MEMORY
   //
  
   //instruction cache instance
   ext_mem ext_mem0 
     (
     .clk                  (clk ),
     .rst                  (reset_int),

     // instruction bus
  .i_req                (int_mem_i_req),
     .i_resp               (int_mem_i_resp),

     //data bus
     .d_req                (int_mem_d_req),
     .d_resp               (int_mem_d_resp)

      //AXI INTERFACE 
      //address write
      .AW_ID(m_axi_awid), 
      .AW_ADDR(m_axi_awaddr), 
      .AW_LEN(m_axi_awlen), 
      .AW_SIZE(m_axi_awsize), 
      .AW_BURST(m_axi_awburst), 
      .AW_LOCK(m_axi_awlock), 
      .AW_CACHE(m_axi_awcache), 
      .AW_PROT(m_axi_awprot),
      .AW_QOS(m_axi_awqos), 
      .AW_VALID(m_axi_awvalid), 
      .AW_READY(m_axi_awready), 

      //write
      .W_DATA(m_axi_wdata), 
      .W_STRB(m_axi_wstrb), 
      .W_LAST(m_axi_wlast), 
      .W_VALID(m_axi_wvalid), 
      .W_READY(m_axi_wready), 

      //write response
      .B_ID(m_axi_bid), 
      .B_RESP(m_axi_bresp), 
      .B_VALID(m_axi_bvalid), 
      .B_READY(m_axi_bready), 
      
      //address read
      .AR_ID(m_axi_arid), 
      .AR_ADDR(m_axi_araddr), 
      .AR_LEN(m_axi_arlen), 
      .AR_SIZE(m_axi_arsize), 
      .AR_BURST(m_axi_arburst), 
      .AR_LOCK(m_axi_arlock), 
      .AR_CACHE(m_axi_arcache), 
      .AR_PROT(m_axi_arprot), 
      .AR_QOS(m_axi_arqos), 
      .AR_VALID(m_axi_arvalid), 
      .AR_READY(m_axi_arready), 

      //read 
      .R_ID(m_axi_rid), 
      .R_DATA(m_axi_rdata), 
      .R_RESP(m_axi_rresp), 
      .R_LAST(m_axi_rlast), 
      .R_VALID(m_axi_rvalid),  
  .R_READY(m_axi_rready)  
       );
`endif

   //
   // BOOT CONTROLLER
   //

   boot_ctr boot0 
     (
      .clk(clk),
      .rst(reset),
      .soft_rst(soft_reset),
      .boot(boot),
      
      //cpu interface
      //no address bus since single address
      .valid(slaves_req[`valid(`BOOT_CTR)]),
      .wdata(slaves_req[`wdata(`BOOT_CTR)]),
      .wstrb(|slaves_req[`wstrb(`BOOT_CTR)]),
      .rdata(slaves_req[`rdata(`BOOT_CTR)]),
      .ready(slaves_req[`ready(`BOOT_CTR)])
   );

   //
   // UART
   //

   iob_uart uart
     (
      .clk       (clk),
      .rst       (reset_int),
      
      //cpu interface
      .valid(slaves_req[`valid(`UART)]),
      .valid(slaves_req[`address_nbits(`UART, `UART_ADDR_W)]),
      .wdata(slaves_req[`wdata(`UART)]),
      .wstrb(|slaves_req[`wstrb(`UART)]),
      .rdata(slaves_req[`rdata(`UART)]),
      .ready(slaves_req[`ready(`UART)])
  
                 
      //RS232 interface
      .txd       (uart_txd),
      .rxd       (uart_rxd),
      .rts       (uart_rts),
      .cts       (uart_cts)
      );

endmodule
